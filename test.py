import subprocess
import os
import time
import psutil


# path = os.path.abspath("exec/snap-ai/")

# print(path)

a = subprocess.Popen(["Kinect_InteractiveObjects.exe"], cwd="exec/kinect/", shell=True)

time.sleep(5)

# a.terminate()
# a.kill()

for child in psutil.Process(a.pid).children(recursive=True):
    child.kill()
a.kill()