from tkinter import *
from tkinter.ttk import *
# from tkinter import messagebox
import subprocess as sp
from PIL import Image, ImageTk
import os
import psutil

top = Tk()

style = Style() 
style.configure('W.TButton', font =
               ('calibri', 24, 'bold'), 
                foreground = 'black')

buttons = []

class SnapButton:
    def __init__(self, image, exe, wd):
        self.name = image.split(".")[0]
        self.image = ImageTk.PhotoImage(Image.open("img/" + image).resize((60, 50)))
        self.btn = Button(top, text="START", style='W.TButton', command=self.toggle, image = self.image, compound="left")
        self.btn.pack(pady=5)
        self.exe = exe
        self.wd = wd
        self.process = None
    def toggle(self):
        if self.btn.config('text')[-1] == 'START':
            self.btn.config(text='STOP')
            self.process = sp.Popen([self.exe], cwd=self.wd, shell=True)
        else:
            self.btn.config(text='START')
            for child in psutil.Process(self.process.pid).children(recursive=True):
                child.kill()
            self.process.kill()
            self.process = None
            # or from windows cmd prompt: taskkill /IM "process name" /F

def kill_webservers():
    global buttons
    for b in buttons:
        if (b.process is not None):
            print("Killing " + b.name)        
            for child in psutil.Process(b.process.pid).children(recursive=False):
                child.kill()
                # child.wait()
            b.process.kill()
            # b.process.wait()

# def helloCallBack():
#     messagebox.showinfo( "Snap link", "Snap link started")

def on_closing():
    global top
    kill_webservers()
    top.destroy()

# def toggle():
#     global webservers
#     if thymio_btn.config('text')[-1] == 'START':
#         thymio_btn.config(text='STOP')
#         webservers["ai"] = sp.Popen(["exec/snap-ai/snap_server.exe"])
#         webservers["thymio"] = sp.Popen(["exec/snap-thymio/asebascratch.exe"])
#     else:
#         thymio_btn.config(text='START')
#         kill_webservers()


#IMAGES
# thymio_img = ImageTk.PhotoImage(Image.open("img/thymio.png").resize((50, 50)))
# img = Label(image=thymio_img)
# img.image = thymio_img
# img.place(x=0, y=0)


#BUTTONS
# B = Button(top, text ="Start", command = helloCallBack)
# # B.pack()
# thymio_btn = Button(text="START", width=12, command=toggle, image = thymio_img, compound="left")
# thymio_btn.pack(pady=5)




# ai = SnapButton("ai.png", "snap_server.exe", "exec/snap-ai/")
# thymio = SnapButton("thymio.png", "asebascratch.exe", "exec/snap-thymio/")
# buttons.append(ai)
# buttons.append(thymio)

print("Opening config file")
config = open("config.csv", "r")
print("Creating buttons")
for line in config.readlines():
    print(line)
    parts = line.split(",")
    btn = SnapButton(str(parts[0]), str(parts[1]), str(parts[2].split("\n")[0]))
    buttons.append(btn)
config.close()
print("Done creating buttons")


#MAIN
top.protocol("WM_DELETE_WINDOW", on_closing)
top.geometry("500x500")
print("Starting main loop")
top.mainloop()