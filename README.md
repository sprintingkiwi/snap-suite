This is a simple tkinter python3 application.

It loads the config.csv file that specifies the buttons to be created with 3 parameters:

* image name (inside img folder, for example: "ai.png")
* executable name (inside exec folder, for example: "snap_server.exe")
* working directory in which the executable file should be launched (for example: "exec/snap-ai/")

You can find the [exec folder](https://bitbucket.org/sprintingkiwi/snap-suite/downloads/exec.zip) (containing all SNAP extension compiled modules) in the Downloads section of this repository.
The available extension modules for now are the compiled version of what you can find in the following repositories:

* [SNAP AI](https://bitbucket.org/sprintingkiwi/snap-tensor) - create a chatbot with SNAP! -> [TUTORIAL](https://sprintingkiwi.bitbucket.io/snap-conversation/snap-conversation.html)(Italian only)
* [SNAP Thymio](https://bitbucket.org/sprintingkiwi/thymio-snap) - program the [Thymio](http://www.thymio.it/) robot with SNAP! -> [TUTORIAL](https://sprintingkiwi.bitbucket.io/4teachers/snap-suite.html)(Italian only)
* [SNAP Kinect](https://bitbucket.org/sprintingkiwi/kinectv2-snap) - receive body tracking data inside SNAP! from your [Microsoft Kinect](https://it.wikipedia.org/wiki/Microsoft_Kinect)
* [SNAP GeomBot](https://bitbucket.org/sprintingkiwi/geombot) - program the [PerContare](https://www.percontare.it/) projects's GeomBot for Primary School geometry activities

## Build instructions
* $ set PATH=%PATH%;C:\Windows\System32\downlevel; (Windows only)
* $ pip install pyinstaller==3.3
* $ pyinstaller snap-suite.py
* Copy the "exec" directory, "config.csv" file and the "img" directory inside the "dist/snap-suite/" directory, along with all the builded files

## Downloads:
* [Windows NSIS compiled installer](https://bitbucket.org/sprintingkiwi/snap-suite/downloads/snap-suite-installer.exe)
* [exec folder](https://bitbucket.org/sprintingkiwi/snap-suite/downloads/exec.zip)
* [Portable compile version](https://bitbucket.org/sprintingkiwi/snap-suite/downloads/snap-suite.zip)
